export class Status {
    contract!: string;
  }
  
  export class Product {
    product_name!: string;
  }
  
  export class Client {
    client_name!: string;
    client_url!: string;
    postal_code!: string;
  }
  
  export class Company {
    company_name!: string;
    postal_code!: string;
    company_url!: string;
    products!: Product[];
  }
  
  export class Employee {
    id!: number;
    firstname!: string;
    lastname!: string;
    email!: string;
    phone!: string;
    team!: string;
    dateOfJoining!: string;
    status!: Status[];
    company!: Company;
    client!: Client[];
  }
  