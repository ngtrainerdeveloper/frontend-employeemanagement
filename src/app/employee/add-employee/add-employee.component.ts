import { Component, OnInit } from '@angular/core';
import { Status } from 'src/app/model/employee.modal';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit{
  visible!:boolean;

  status: Status[] = [];
  selectedStatus!: Status;

  value!: string;
  phoneNumber!: string;
  firstName!:string;
  lastName!: string;
  emailId!: string;
  teamName!: string;
  date!: Date;
  companyName!: string;
  postalCode!: string;
  companyURL!:string;
  clientName!: string;

  constructor(){};

  ngOnInit(): void {
    this.status = [
      {contract: 'ACTIVE'},
      {contract: 'NOT-ACTIVE'}
    ];

    
  }

  showDialog() {
      this.visible = !this.visible;
  }
}
