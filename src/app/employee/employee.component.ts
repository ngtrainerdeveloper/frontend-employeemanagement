import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/employee.modal';
import { EmployeeServiceService } from '../employee-service.service';
import { interval } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']})
export class EmployeeComponent implements OnInit{
  employees: Employee[] = [];

  constructor(private empService: EmployeeServiceService) {};

  ngOnInit(): void {
    this.loadData();

    // The interval() function creates an observable that emits a value every 10 seconds. 
    //The switchMap() operator is used to switch to the latest observable and retrieve the latest data from the server.
    interval(100).pipe(
      switchMap(() => this.empService.getEmployees())
    ).subscribe(data => {
      this.employees = data;
    });
  }


  loadData(){
    this.empService.getEmployees()
    .subscribe(
      data => {
        this.employees = data;
    },
    error => {
      console.log(error);
    });
  }
}
