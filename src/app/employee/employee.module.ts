import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DividerModule } from 'primeng/divider';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { ChipsModule } from 'primeng/chips';



@NgModule({
  declarations: [EmployeeComponent, AddEmployeeComponent],
  imports: [
    CommonModule,CalendarModule, FormsModule,TableModule,ButtonModule,DialogModule,DividerModule,DropdownModule,InputTextModule,InputMaskModule, ChipsModule
  ],
  exports: [EmployeeComponent],
  providers: []
})
export class EmployeeModule { }
