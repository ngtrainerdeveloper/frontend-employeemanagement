import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Employee} from '../app/model/employee.modal';
import {environment} from '../app/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  private baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  //Get All Employees
  getEmployees(): Observable<Employee[]>{
    return this.http.get<Employee[]>(`${this.baseUrl}`)
  }
}
